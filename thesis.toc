\select@language {english}
\select@language {german}
\select@language {english}
\contentsline {chapter}{Acknowledgments}{i}{chapter*.2}
\contentsline {chapter}{Disclaimer}{v}{chapter*.3}
\contentsline {chapter}{Declaration of co-authorship}{vii}{chapter*.4}
\contentsline {chapter}{Abstract}{ix}{chapter*.6}
\select@language {german}
\contentsline {chapter}{Zusammenfassung}{xiii}{chapter*.7}
\select@language {english}
\select@language {english}
\contentsline {chapter}{List of Figures}{xxi}{chapter*.8}
\contentsline {chapter}{List of Tables}{xxiv}{chapter*.9}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Context and motivation}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Background}{3}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Theoretical background on crime concentrations}{4}{subsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.1.1}Spatio-temporal crime concentrations}{4}{subsubsection.1.2.1.1}
\contentsline {subsubsection}{\numberline {1.2.1.2}Theories of crime}{4}{subsubsection.1.2.1.2}
\contentsline {subsection}{\numberline {1.2.2}Spatio-temporal crime modeling}{7}{subsection.1.2.2}
\contentsline {subsubsection}{\numberline {1.2.2.1}Methods}{8}{subsubsection.1.2.2.1}
\contentsline {subsubsection}{\numberline {1.2.2.2}Data}{11}{subsubsection.1.2.2.2}
\contentsline {subsection}{\numberline {1.2.3}Urban computing}{13}{subsection.1.2.3}
\contentsline {section}{\numberline {1.3}Objective and approach}{15}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Advancing long-term crime prediction}{16}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Advancing theory testing}{17}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Advancing short-term crime prediction}{18}{subsection.1.3.3}
\contentsline {section}{\numberline {1.4}Thesis outline}{19}{section.1.4}
\contentsline {chapter}{\numberline {2}Leveraging human activity data for long-term crime prediction in high population density areas}{21}{chapter.2}
\contentsline {section}{\numberline {2.1}Introduction}{21}{section.2.1}
\contentsline {section}{\numberline {2.2}Related work}{24}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Human activity data}{24}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Long-term crime prediction}{24}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Research gap and contributions}{26}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Methods and materials}{27}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Data}{27}{subsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.1.1}Crime data}{28}{subsubsection.2.3.1.1}
\contentsline {subsubsection}{\numberline {2.3.1.2}Census data}{28}{subsubsection.2.3.1.2}
\contentsline {subsubsection}{\numberline {2.3.1.3}Foursquare venues data}{29}{subsubsection.2.3.1.3}
\contentsline {subsubsection}{\numberline {2.3.1.4}Subway usage data}{30}{subsubsection.2.3.1.4}
\contentsline {subsubsection}{\numberline {2.3.1.5}Taxi usage data}{30}{subsubsection.2.3.1.5}
\contentsline {subsection}{\numberline {2.3.2}Spatio-temporal discretization}{31}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Feature generation}{32}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Model specification}{37}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}Results}{39}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Descriptive statistics}{39}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Prediction performance}{40}{subsection.2.4.2}
\contentsline {subsubsection}{\numberline {2.4.2.1}Geographical evaluation}{44}{subsubsection.2.4.2.1}
\contentsline {subsubsection}{\numberline {2.4.2.2}Temporal evaluation}{45}{subsubsection.2.4.2.2}
\contentsline {subsection}{\numberline {2.4.3}Relevance of features}{45}{subsection.2.4.3}
\contentsline {subsubsection}{\numberline {2.4.3.1}Feature importance}{45}{subsubsection.2.4.3.1}
\contentsline {subsubsection}{\numberline {2.4.3.2}Partial dependence plots}{47}{subsubsection.2.4.3.2}
\contentsline {subsection}{\numberline {2.4.4}Geographical error}{50}{subsection.2.4.4}
\contentsline {section}{\numberline {2.5}Discussion}{50}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Interpretation and link to the literature}{50}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Implications for research and practice}{53}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}Limitations and potential for future research}{54}{subsection.2.5.3}
\contentsline {section}{\numberline {2.6}Conclusion}{55}{section.2.6}
\contentsline {chapter}{\numberline {3}Leveraging human mobility data for testing crime pattern theory at scale}{57}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduction}{58}{section.3.1}
\contentsline {section}{\numberline {3.2}Related Work}{60}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Theoretical background}{60}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Geo-tagged data for modeling crime}{61}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Mining data from online location services}{62}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Research gap and contributions}{63}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Methods and materials}{63}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Data}{63}{subsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.1.1}Crime dataset}{65}{subsubsection.3.3.1.1}
\contentsline {subsubsection}{\numberline {3.3.1.2}Foursquare dataset with user transitions}{65}{subsubsection.3.3.1.2}
\contentsline {subsection}{\numberline {3.3.2}Feature generation}{66}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Computation of pass-through transitions}{68}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Model specification}{71}{subsection.3.3.4}
\contentsline {subsubsection}{\numberline {3.3.4.1}Empirical modeling for theory testing}{71}{subsubsection.3.3.4.1}
\contentsline {subsubsection}{\numberline {3.3.4.2}Predictive modeling for crime forecasting}{72}{subsubsection.3.3.4.2}
\contentsline {section}{\numberline {3.4}Results}{73}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Descriptive statistics}{73}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Empirical relationship between mobility flows and crime}{75}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Predictive power of mobility flows in crime forecasting}{76}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Sensitivity analysis by activity types}{79}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Sensitivity analysis by crime types}{80}{subsection.3.4.5}
\contentsline {subsection}{\numberline {3.4.6}Robustness checks}{81}{subsection.3.4.6}
\contentsline {section}{\numberline {3.5}Discussion}{81}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Contributions to theory}{81}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Implications for research and practice}{82}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Limitations and potential for future research}{83}{subsection.3.5.3}
\contentsline {section}{\numberline {3.6}Conclusion}{83}{section.3.6}
\contentsline {chapter}{\numberline {4}Developing imbalance-aware machine learning for short-term crime prediction in low population areas}{85}{chapter.4}
\contentsline {section}{\numberline {4.1}Introduction}{86}{section.4.1}
\contentsline {section}{\numberline {4.2}Related work}{88}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Theoretical foundation}{88}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Short-term crime prediction}{90}{subsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.1}Na{\"i}ve predictions from historic crime data}{90}{subsubsection.4.2.2.1}
\contentsline {subsubsection}{\numberline {4.2.2.2}Machine learning models with spatio-temporal predictors}{90}{subsubsection.4.2.2.2}
\contentsline {subsection}{\numberline {4.2.3}Research gap and contributions}{91}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Methods and materials}{93}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Research framework}{93}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Methods for imbalance-aware machine learning}{94}{subsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.2.1}Proposed hyper-ensemble}{94}{subsubsection.4.3.2.1}
\contentsline {subsubsection}{\numberline {4.3.2.2}Baselines}{95}{subsubsection.4.3.2.2}
\contentsline {subsection}{\numberline {4.3.3}Estimation procedure}{95}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Prediction performance}{96}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}Data}{97}{subsection.4.3.5}
\contentsline {section}{\numberline {4.4}Results}{102}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Prediction performance}{102}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Sensitivity of prediction to base learners}{103}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Sensitivity of prediction by population density}{103}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}Sensitivity of prediction to temporal resolution}{107}{subsection.4.4.4}
\contentsline {subsection}{\numberline {4.4.5}Relevance of feature sets}{107}{subsection.4.4.5}
\contentsline {section}{\numberline {4.5}Discussion}{108}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Interpretation and link to the literature}{108}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Implications for research and practice}{110}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Limitations and potential for future research}{111}{subsection.4.5.3}
\contentsline {section}{\numberline {4.6}Conclusion}{111}{section.4.6}
\contentsline {chapter}{\numberline {5}General discussion and conclusion}{113}{chapter.5}
\contentsline {section}{\numberline {5.1}Summary and key findings for research and practice}{113}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Advancing long-term crime prediction}{113}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Advancing theory testing}{115}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Advancing short-term crime prediction}{117}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Limitations and research outlook}{119}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Latent Gaussian processes to estimate the influence of criminogenic places on crime}{121}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Supervised latent Dirichlet allocation to estimate the influence of mobility motifs on crime}{123}{subsection.5.2.2}
\contentsline {section}{\numberline {5.3}Conclusion}{124}{section.5.3}
\contentsline {chapter}{Glossary}{127}{section*.46}
\contentsline {chapter}{Appendices}{129}{section*.48}
\contentsline {chapter}{\numberline {A}Appendix for \Cref {chapter:predict_long}}{131}{Appendix.a.A}
\contentsline {chapter}{\numberline {B}Appendix for \Cref {chapter:predict_short}}{133}{Appendix.a.B}
\contentsline {chapter}{Bibliography}{133}{figure.caption.50}
